## Результаты лабораторных работ:
* [Практика 0](reports/practice0.md)
* [Практика 1](reports/practice1.md)
* [Практика 2](reports/practice2.md)
* Практика 3 - см. скрипты
* [Практика 4](reports/practice4.md)


## Дополнительно

### Как пользоваться [environment.yml](environment.yml)
Данный файл предназначен для автоматического развёртывания окружения Conda со всем необходимыми для практик пакетами (подробнее [здесь](https://conda.io/en/latest/user-guide/getting-started.html#managing-environments))

Для создания окружения требуется любой выпуск Conda (Anaconda или Miniconda) с минимальной конфигурацией и возможностью управления через терминал.

Следующая команда создаст окружение ```NLP4IS``` с предустановленными пакетами:

``` bash
$ conda env create -f environment.yml
```

Для обновления окружения можно воспользоваться командой:

``` bash
$ conda env update -f environment.yml
```
