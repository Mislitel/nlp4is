
"""
    This is the central config file
    @author: Gerhard Wohlgenannt (2017), ITMO University, St.Petersburg, Russia

    Here you can change pathes, add models, add new BOOK_SERIES, new dataset (towards the end of the file).
    But just to start with existing datasets and models, no change is needed

    Modified by Mislitel
"""

#TODO: Привести в порядок пути
#TODO: При обучении с mystem doesnt_match не работает из-за регистра символов - исправить это

#BOOK_SERIES="ASIOF"
BOOK_SERIES="WAPRU"    # War and peace (Russian)
#BOOK_SERIES="WAPEN"    # War and peace (English)

MODEL_PATH="../models/"


if BOOK_SERIES == "ASIOF":
    ## if you have a binary model, set the second value to "bin", else to "vec"
    METHODS = [ 
        ('asoif_fastText', 'vec'), # default and: -epoch 25 -ws 12
    ]
elif BOOK_SERIES == "WAPRU":
    MODEL_PATH = "../../NLP4IS/data/practice3/"
    METHODS = [('war-and-peace-ru', 'vec')]
elif BOOK_SERIES == "WAPEN":
    MODEL_PATH = "../../NLP4IS/data/practice3/"
    METHODS = [('war-and-peace-en', 'vec')]

# -----------------------------------------------------
# for "doesnt_match" evaluation script
# -----------------------------------------------------

if BOOK_SERIES == "ASIOF":
    PRINT_DETAILS = False ## verbose debugging of eval results

    DOESNT_MATCH_FILE = "../datasets/questions_soiaf_doesnt_match.txt"
    ANALOGIES_FILE = "../datasets/questions_soiaf_analogies.txt"

    ### which sections to show in the paper..
    ANALOGIES_SECTIONS = ['firstname-lastname', 'child-father', 'husband-wife', 'geo-name-location', 'houses-seats', 'total']
    DOESNT_MATCH_SECTIONS = [': family-siblings',  ': names-of-houses', ': archmaesters', ': rivers', ': free cities', 'TOTAL']

elif BOOK_SERIES == "WAPRU":
    PRINT_DETAILS = True

    DOESNT_MATCH_FILE = MODEL_PATH + "questions_w_and_p_doesnt_match_ru.txt"
    ANALOGIES_FILE = MODEL_PATH + "questions_w_and_p_analogies_ru.txt"

    ANALOGIES_SECTIONS = ['firstname-lastname', 'title-lastname', 'child-parent', 'staff-master', 'total']
    DOESNT_MATCH_SECTIONS = [': emperors',  ': military-men', ': Bolkinski family', ': Rostov family', 'TOTAL']
elif BOOK_SERIES == "WAPEN":
    PRINT_DETAILS = True

    DOESNT_MATCH_FILE = MODEL_PATH + "questions_w_and_p_doesnt_match_en.txt"
    ANALOGIES_FILE = MODEL_PATH + "questions_w_and_p_analogies_en.txt"

    ANALOGIES_SECTIONS = ['firstname-lastname', 'title-lastname', 'child-parent', 'staff-master', 'total']
    DOESNT_MATCH_SECTIONS = [': emperors',  ': military-men', ': Bolkinski family', ': Rostov family', 'TOTAL']
