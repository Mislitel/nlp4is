Script [positional_index.py](../python/practice2/positional_index.py) output:

```
*** Introductory Examples for the NLTK Book ***
Loading text1, ..., text9 and sent1, ..., sent9
Type the name of the text or sentence to view it.
Type: 'texts()' or 'sents()' to list the materials.
text1: Moby Dick by Herman Melville 1851
text2: Sense and Sensibility by Jane Austen 1811
text3: The Book of Genesis
text4: Inaugural Address Corpus
text5: Chat Corpus
text6: Monty Python and the Holy Grail
text7: Wall Street Journal
text8: Personals Corpus
text9: The Man Who Was Thursday by G . K . Chesterton 1908
With stopwords:
The Hound of the Baskervilles [(',', 3580), ('the', 3244), ('.', 2627), ('of', 1696), ('and', 1583), ('I', 1497), ('to', 1456), ('�', 1413), ('�', 1377), ('a', 1286), ('that', 1090), ('in', 921), ('was', 794), ('it', 761), ('you', 754), ('he', 724), ('his', 661), ('is', 627), ('have', 530), ('?', 518), ('had', 502), ('with', 468), ('which', 424), ('for', 423), ('my', 421), ('�', 412), ('not', 411), ('as', 387), ('we', 351), ('at', 349), ('be', 346), ('this', 342), ('upon', 316), ('me', 312), ('him', 307), ('from', 285), ('but', 268), ('The', 260), ('Sir', 248), ('said', 240), ('s', 236), ('It', 226), ('been', 222), ('all', 221), ('on', 220), ('one', 220), ('by', 212), ('so', 211), ('are', 211), ('man', 209)]
Moby Dick [(',', 18713), ('the', 13721), ('.', 6862), ('of', 6536), ('and', 6024), ('a', 4569), ('to', 4542), (';', 4072), ('in', 3916), ('that', 2982), ("'", 2684), ('-', 2552), ('his', 2459), ('it', 2209), ('I', 2124), ('s', 1739), ('is', 1695), ('he', 1661), ('with', 1659), ('was', 1632), ('as', 1620), ('"', 1478), ('all', 1462), ('for', 1414), ('this', 1280), ('!', 1269), ('at', 1231), ('by', 1137), ('but', 1113), ('not', 1103), ('--', 1070), ('him', 1058), ('from', 1052), ('be', 1030), ('on', 1005), ('so', 918), ('whale', 906), ('one', 889), ('you', 841), ('had', 767), ('have', 760), ('there', 715), ('But', 705), ('or', 697), ('were', 680), ('now', 646), ('which', 640), ('?', 637), ('me', 627), ('like', 624)]
Without stopwords:
The Hound of the Baskervilles [('upon', 316), ('Sir', 248), ('said', 240), ('one', 220), ('man', 209), ('could', 198), ('would', 188), ('Holmes', 187), ('us', 175), ('Henry', 153), ('moor', 145), ('know', 116), ('may', 111), ('Watson', 111), ('see', 110), ('Baskerville', 110), ('must', 96), ('Charles', 94), ('Stapleton', 93), ('sir', 91), ('Mortimer', 89), ('think', 85), ('might', 85), ('Project', 82), ('time', 80), ('two', 77), ('much', 76), ('come', 76), ('face', 76), ('back', 73), ('Barrymore', 73), ('came', 72), ('eyes', 71), ('last', 71), ('night', 69), ('say', 69), ('long', 67), ('heard', 65), ('old', 65), ('work', 64), ('way', 62), ('tell', 61), ('friend', 61), ('saw', 61), ('never', 60), ('hound', 60), ('away', 59), ('death', 59), ('case', 59), ('Well', 56)]
Moby Dick [('whale', 906), ('one', 889), ('like', 624), ('upon', 538), ('man', 508), ('ship', 507), ('Ahab', 501), ('ye', 460), ('old', 436), ('sea', 433), ('would', 421), ('head', 335), ('though', 335), ('boat', 330), ('time', 324), ('long', 318), ('said', 302), ('yet', 300), ('still', 299), ('great', 293), ('two', 285), ('seemed', 283), ('must', 282), ('Whale', 282), ('last', 277), ('way', 269), ('Stubb', 255), ('see', 253), ('Queequeg', 252), ('little', 247), ('round', 242), ('whales', 237), ('say', 237), ('three', 237), ('men', 236), ('thou', 232), ('may', 230), ('us', 228), ('every', 222), ('much', 218), ('could', 215), ('Captain', 215), ('first', 210), ('side', 208), ('hand', 205), ('ever', 203), ('Starbuck', 196), ('never', 195), ('good', 192), ('white', 191)]
-------------------------------------------------------------


Searching example:
phrase 'Sir Henry Baskerville': {35: [[168, 169, 170]], 36: [[92, 93, 94]], 37: [[2, 3, 4]], 38: [[2, 3, 4]], 40: [[84, 85, 86]], 45: [[0, 1, 2]], 53: [[148, 149, 150]], 55: [[82, 83, 84]], 69: [[9, 10, 11]], 74: [[196, 197, 198]], 205: [[62, 63, 64]], 226: [[298, 299, 300]], 110: [[101, 102, 103]]}
phrase 'Sherlock Holmes'{0: [[100, 101], [102, 101]], 4: [[152, 153]], 6: [[122, 123]], 15: [[205, 206]], 153: [[87, 88]], 25: [[20, 21]], 36: [[106, 107]], 167: [[143, 144]], 44: [[162, 163]], 53: [[76, 77]], 63: [[13, 14]], 69: [[166, 167]], 87: [[154, 155]], 218: [[32, 33]], 91: [[209, 210]], 228: [[7, 8]], 229: [[60, 61]], 231: [[102, 103]], 232: [[50, 51]], 109: [[50, 51]]}
phrase 'Holmes said': {5: [[54, 53]], 40: [[82, 83]], 50: [[153, 152]], 51: [[158, 157]], 54: [[46, 45]], 72: [[14, 13]], 215: [[12, 11]], 219: [[42, 41]], 222: [[29, 30]], 224: [[147, 148]], 233: [[107, 106]], 240: [[6, 5]], 244: [[21, 20]], 246: [[6, 5]], 249: [[124, 123]], 254: [[93, 92]], 256: [[150, 149]]}
```

