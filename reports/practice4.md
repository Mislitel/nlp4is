Script [stupid_corefs.py](../python/practice4/stupid_corefs.py) output:

```
Original text:

The big clock on the tower of the Palace of Westminster in London is often called Big Ben. But Big Ben is really the bell of the clock. It is the biggest clock bell in Britain. It weighs 13.5 tons.
The clock tower is 318 feet high. You have to go up 374 steps to reach the top. So the clock looks small from the pavement below the tower.
But its face is 23 feet wide. It would only just fit into some classrooms.
The minute-hand is 14 feet long. Its weight is equal to that of two bags of coal. The hour-hand is 9 feet long.
The clock bell is called Big Ben after Sir Benjamin Hall. He had the job to see that the bell was put up.
Sir Benjamin was a big man. One day he said in Parliament, "Shall we call the bell St. Stephen's?" St. Stephen's is the name of the tower.
But someone said for a joke, "Why not call it Big Ben?" Now the bell is known all over the world by that name.



Custom corefs:


['It', 'Ben']
['It', 'Britain']
['You', 'Britain']
['It', '374']
['He', 'Benjamin']
['he', 'Benjamin']
['we', 'Parliament']
['it', 'Parliament']

Correct: 3
Incorrect: 5
Total: 8

Score: 0.375


Spacy corefs:
['Ben', 'Big Ben']
['It', 'It']
['It', 'It']
['its', 'The big clock on the tower of the Palace of Westminster in London']
['It', 'The big clock on the tower of the Palace of Westminster in London']
['Its', 'The big clock on the tower of the Palace of Westminster in London']
['He', 'Sir Benjamin Hall']
['he', 'Sir Benjamin']

Correct: 4
Incorrect: 4
Total: 8

Score: 0.5
```
