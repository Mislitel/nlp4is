Вывод скрипта [simple_grammar_homonymy](../python/practice1/simple_grammar_homonymy.py):

```
(S
  (ГР_ПОДЛ (ОПР (МЕСТ Эти)) (ПОДЛ (СУЩ типы)) (ДОП (СУЩ стали)))
  (ГР_СКАЗ (СКАЗ (ГЛ есть)) (ОБСТ (ПР в) (СУЩ цехе))))
(S
  (ГР_ПОДЛ (ОПР (МЕСТ Эти)) (ПОДЛ (СУЩ типы)))
  (ГР_СКАЗ (СКАЗ (ГЛ стали) (ГЛ есть)) (ОБСТ (ПР в) (СУЩ цехе))))
```

