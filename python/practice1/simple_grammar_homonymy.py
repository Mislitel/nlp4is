#!/usr/bin/env python
import nltk

groucho_grammar_ru = nltk.CFG.fromstring("""
S -> ГР_ПОДЛ ГР_СКАЗ
ГР_ПОДЛ -> ПОДЛ | ОПР ПОДЛ | ОПР ПОДЛ ДОП
ГР_СКАЗ -> СКАЗ | СКАЗ ОБСТ
ПОДЛ -> СУЩ
ОПР -> МЕСТ
ДОП -> СУЩ
СКАЗ -> ГЛ | ГЛ ГЛ
ОБСТ -> ПР СУЩ
СУЩ -> 'типы' | 'стали' | 'цехе'
ГЛ -> 'стали' | 'есть'
МЕСТ -> 'Эти'
ПР -> 'в'
""")


def demo():
    sent_ru = ['Эти', 'типы', 'стали', 'есть', 'в', 'цехе']
    parser_ru = nltk.ChartParser(groucho_grammar_ru)
    for tree in parser_ru.parse(sent_ru):
        print(tree)

if __name__ == "__main__":
    demo()
