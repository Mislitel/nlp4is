#!/usr/bin/env python
import gensim
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize, sent_tokenize
from pymystem3 import Mystem

def train_model(filename, russian_text=True, russian_lemmatization=False):
    with open(filename, 'r') as file:
        raw_text = file.read()
    if russian_text and russian_lemmatization:
        mystem = Mystem()   # lemmatizator
    text = []
    for sentence in sent_tokenize(raw_text):
        if russian_text and russian_lemmatization:
            text.append([token for token in mystem.lemmatize(sentence) if token.isalpha() and token.lower() not in stopwords.words('russian')])
        elif russian_text:
            text.append([token for token in word_tokenize(sentence) if token.isalpha() and token.lower() not in stopwords.words('russian')])
        else:
            text.append([token for token in word_tokenize(sentence) if token.isalpha() and token.lower() not in stopwords.words('english')])

    return gensim.models.Word2Vec(text, min_count=5, size=300, workers=4, window=10, sg=1, negative=5, iter=5)


def demo():
    if __name__ == "__main__":
        data_prefix = "../../data/practice3"
    else:
        data_prefix = "../data/practice3"

    en_data = '../../../nlp4is_word_embeddings/student_datasets/war-and-peace.txt'
    ru_data = data_prefix + '/war-and-peace-ru.txt'

    print("Training model for English...")
    model = train_model(en_data, russian_text=False)
    model.wv.save_word2vec_format(data_prefix + "/war-and-peace-en.model", binary=False)

    print("Training model for Russian...")
    model = train_model(ru_data, russian_text=True)
    model.wv.save_word2vec_format(data_prefix + "/war-and-peace-ru.model", binary=False)

    print("Training model for Russian (with lemmatization)...")
    model = train_model(ru_data, russian_text=True, russian_lemmatization=True)
    model.wv.save_word2vec_format(data_prefix + "/war-and-peace-ru-lemm.model", binary=False)

if __name__ == '__main__':
    demo()

