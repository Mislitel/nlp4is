#!/usr/bin/env python
import re
re_data_string_pattern = re.compile(r"(.*)\t(.*)\t(.*)\t(.*)\t(.*)")
re_post_processing_pattern = re.compile(r"((?P<presymbol>[«\(])\s|\s(?P<postsymbol>[.,;:»\)]))")


def parse_data_string(line):
    if line == '</s>\n':
        return '\n'
    match = re_data_string_pattern.search(line)
    if match is None:
        return ''
    return match.group(1) + ' '


def post_processing(text):
    return re_post_processing_pattern.sub('\g<presymbol>\g<postsymbol>', text)


if __name__ == '__main__':
   data_prefix = '../../data/practice0'
else:
   data_prefix = '../data/practice0'

text = ''
print("Processing...")
with open(data_prefix + "/ru_ar_cut.txt", "r") as file:
    for line in file:
        text += parse_data_string(line)

print('Post-processing...')
text = post_processing(text)

print("Writing to file...")

with open(data_prefix + "/ru_ar_cut_text.txt", "w") as file:
    file.write(text)
print("Ok")
