#!/usr/bin/env python
# -*- coding: utf-8 -*-

import math
import re
import nltk
import os
import ast
import operator

from nltk.collocations import TrigramCollocationFinder
from nltk import ngrams
from nltk import word_tokenize
from functools import reduce

class NGCalculator:
    @staticmethod
    def is_word(token):
        return NGCalculator.__re_correct_word.fullmatch(token)

    def score_ngrams_mi(self):
        # расчёт mi, сортировка по величине mi и по алфавиту
        return sorted({ words: self.mi(words) for words in self.__ngrams.keys() }.items(), key=(lambda item: (-item[1], item[0])))

    def N(self):
        return len(self.__words)

    def f(self, subject):
        if isinstance(subject, str):
            return self.__dictionary[subject]
        else:
            return self.__ngrams[subject]

    def mi(self, words):
        # формула взята из кода nltk
        return math.log2( (self.f(words) * self.N() ** (self.__ngram_size - 1)) / (reduce((lambda x, y: x * y), [self.f(w) for w in list(words)])) )

    def words(self):
        return self.__words

    __re_data_string_pattern = re.compile(r"(.*)\t(.*)\t(.*)\t(.*)\t(.*)")
    __re_post_processing_pattern = re.compile(r"((?P<presymbol>[«\(])\s|\s(?P<postsymbol>[.,;:»\)]))")
    __re_correct_word = re.compile(r"[^\W\d_]+|[^\W\d_]+-[^\W\d_]+")

    def __init__(self, datafile_path, ngram_size=3, is_lex_analyse=True):
        self.__N = 0
        self.__words = []
        self.__dictionary = {}
        self.__datafile_path = datafile_path
        self.__is_lex_analyse = is_lex_analyse
        self.__ngram_size = ngram_size
        self.__ngrams = {}
        self.__get_lexems()

    def __parse_data_line(self, line):
        if line == '</s>\n':
            return '\n'
        match = self.__re_data_string_pattern.search(line)
        if match is None:
            return ''

        # если анализ по лексемам, берём начальные формы слова (2 столбец),
        # если по словоформам - оригинальные (1 столбец)
        if self.__is_lex_analyse:
            return match.group(2) + ' '
        else:
            return match.group(1) + ' '

    def __post_processing(self, text):
        return self.__re_post_processing_pattern.sub('\g<presymbol>\g<postsymbol>', text)


    def __get_lexems(self):
        if self.__is_lex_analyse:
            print("Lex analysis")
            print("Processing...")
            text = ''
            with open(self.__datafile_path, "r") as file:
                for line in file:
                    text += self.__parse_data_line(line)

            print("Post-processing...")
            text = self.__post_processing(text)

            print("Tokenizing...")
            tokens = [token.lower() for token in nltk.word_tokenize(text) if self.is_word(token)]

            print("n-grams building...")
            for ngram in ngrams(tokens, self.__ngram_size):
                if ngram not in self.__ngrams.keys():
                    self.__ngrams[ngram] = 1
                else:
                    self.__ngrams[ngram] += 1

            print("Filling dictionary...")
            self.__fill_words_and_dictionary(tokens)
            print("Ok")
        else:
            raise Exception("Word form analysis is not implemented")
        return

    def __fill_words_and_dictionary(self, tokens):
        for token in tokens:
            if self.is_word(token):
                self.__words.append(token)
                if token in self.__dictionary.keys():
                    self.__dictionary[token] += 1
                else:
                    self.__dictionary[token] = 1
        self.__N = len(self.__dictionary)
        return


def demo():
    if __name__ == "__main__":
        data_prefix = '../../data/practice0'
    else:
        data_prefix = '../data/practice0'

    nc = NGCalculator(data_prefix + '/ru_ar_cut.txt')

    # триграммы с самописным расчётом метрики
    print("Custum-scored trigrams:")
    for ngram in nc.score_ngrams_mi()[:30]:
        print(ngram)
    print('\n')

    trigram_measures = nltk.collocations.TrigramAssocMeasures()
    finder = TrigramCollocationFinder.from_words(nc.words())

    # триграммы с расчётом метрики из nltk
    print('nltk-scored trigrams:')
    for ngram in finder.score_ngrams(trigram_measures.pmi)[:30]:
        print(ngram)

if __name__ == "__main__":
    demo()

