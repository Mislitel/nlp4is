#!/usr/bin/env python

import sys
from lxml import html

if len(sys.argv) != 2:
    print("Usage: " + sys.argv[0] + " <html for analysis>")
    exit(-1)

with open(sys.argv[1]) as f:
    raw_text = f.read()

tree = html.fromstring(raw_text)
title = tree.xpath('//title')[0]
print(title.text_content())
