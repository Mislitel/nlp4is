#!/usr/bin/env python
import re
from nltk.tokenize import word_tokenize
re_correct_word = re.compile(r"[^\W\d_]+|[^\W\d_]+-[^\W\d_]+")


def is_word(token):
    return re_correct_word.fullmatch(token)


if __name__ == "__main__":
    data_prefix = '../../data/practice0'
else:
    data_prefix = '../data/practice0'

filename = data_prefix + '/ru_ar_cut_text.txt'
with open(filename, 'rt') as file:
    text = file.read()

tokens = word_tokenize(text)
words_literals = [token for token in tokens if is_word(token)]
print(words_literals)

#with open(data_prefix + '/ru_ar_cut_preproceed.txt', 'w') as file:
    #file.write(str(words_literals))
