#!/usr/bin/env python
from nltk import Text
from nltk import FreqDist
from nltk.corpus import stopwords
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.book import text1 as moby_dick


class PositionalIndex:
    def __init__(self, data):
        if isinstance(data, list):
            self.__documents = data
        else:
            self.__documents = PositionalIndex.text_preprocessing(data, paragraphs_allowed=True)
        self.__dictionary = {}

        document_id = 0
        for document in self.__documents:
            word_position = 0
            for word in document:
                if word in self.__dictionary.keys():
                    if document_id in self.__dictionary[word].keys():
                        self.__dictionary[word][document_id].append(word_position)
                    else:
                        self.__dictionary[word][document_id] = [word_position]
                else:
                    self.__dictionary[word] = {document_id: [word_position]}
                word_position += 1
            document_id += 1

    def search(self, query):
        query = query.split()
        if not query:
            return []
        result = self.__dictionary[query[0]]
        for i in range(len(query) - 1):
            result = PositionalIndex.positional_intersect(result, self.__dictionary[query[i + 1]], 1)
        return result

    @staticmethod
    def text_preprocessing(text, paragraphs_allowed=False):
        if paragraphs_allowed:
            sentences = sent_tokenize(text)
            paragraphs = []
            paragraph_size = 10
            while len(sentences) > paragraph_size:
                paragraphs.append(PositionalIndex.paragraph_processing(sentences[:paragraph_size]))
                sentences = sentences[paragraph_size:]
            return paragraphs
        else:
            return [token for token in word_tokenize(text)
                    if token.lower() not in stopwords.words('english') and token.isalpha()]

    @staticmethod
    def paragraph_processing(sentences):
        paragraph_tokens = []
        for sentence in sentences:
            for token in word_tokenize(sentence):
                if token.lower not in stopwords.words('english') and token.isalpha():
                    paragraph_tokens.append(token)
        return paragraph_tokens

    # алгоритм взят из Google Disk NLP4IS "books and papers/irbookprint.pdf", page 61
    # (Introduction to Informational Retrieval by Christopher D. Manning (Stanford University), page 39)
    @staticmethod
    def positional_intersect(dict1, dict2, k):
        answer = {}
        intersected_documents = set(dict1.keys()) & set(dict2.keys())
        for document_id in intersected_documents:
            list1 = dict1[document_id]
            list2 = dict2[document_id]
            l = []
            while list1:
                item1 = list1[0] if isinstance(list1[0], int) else list1[0][-1]
                while list2:
                    item2 = list2[0] if isinstance(list2[0], int) else list2[0][0]
                    if abs(item1 - item2) <= k:
                        l.append(list2[0])
                    elif item1 > item2:
                        break
                    list2 = list2[1:]
                l = list(filter(lambda x: abs(x - item1) <= k, l))
                for ps in l:
                    if document_id not in answer.keys():
                        answer[document_id] = []
                    temp = [list1[0]] if isinstance(list1[0], int) else list1[0]
                    temp += [ps] if isinstance(ps, int) else ps
                    answer[document_id].append(temp)
                list1 = list1[1:]
        return answer


def demo():
    if __name__ == "__main__":
        data_prefix = "../../data/practice2"
    else:
        data_prefix = "../data/practice2"

    with open(data_prefix + "/the_hound_of_the_baskervilles.txt", "r") as file:
        text = file.read()
    title_thotb = "The Hound of the Baskervilles"
    title_moby_dick = "Moby Dick"

    most_common_maximum = 50

    tokens = word_tokenize(text)
    the_hound_of_the_baskervilles = Text(tokens)

    print("With stopwords:")
    fd_thotb = FreqDist(the_hound_of_the_baskervilles)
    print(title_thotb + ' ' + str(fd_thotb.most_common(most_common_maximum)))

    fd_md = FreqDist(moby_dick)
    print(title_moby_dick + ' ' + str(fd_md.most_common(most_common_maximum)))

    print("Without stopwords:")
    the_hound_of_the_baskervilles_nsw = [token for token in the_hound_of_the_baskervilles if token.lower() not in stopwords.words('english') and token.isalpha()]
    moby_dick_nsw = [token for token in moby_dick if token.lower() not in stopwords.words('english') and token.isalpha()]

    fd_thotb_nsw = FreqDist(the_hound_of_the_baskervilles_nsw)
    print(title_thotb + ' ' + str(fd_thotb_nsw.most_common(most_common_maximum)))

    fd_md_nsw = FreqDist(moby_dick_nsw)
    print(title_moby_dick + ' ' + str(fd_md_nsw.most_common(most_common_maximum)))
    print("-------------------------------------------------------------")

    pi = PositionalIndex(text)

    print('\n')
    print("Searching example:")
    print("phrase 'Sir Henry Baskerville': " + str(pi.search("Sir Henry Baskerville")))
    print("phrase 'Sherlock Holmes'" + str(pi.search("Sherlock Holmes")))
    print("phrase 'Holmes said': " + str(pi.search("Holmes said")))


if __name__ == "__main__":
    demo()
