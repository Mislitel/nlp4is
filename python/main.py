#!/usr/bin/env python
from nltk.corpus import gutenberg
from practice0.ngcalculator import NGCalculator
from practice0.ngcalculator import demo as demo0
from practice1.simple_grammar_homonymy import demo as demo1
from practice2.positional_index import demo as demo2
from practice4.stupid_corefs import demo as demo4


def show_gutenberg_books_list():
    print("----------------------------")
    for file in gutenberg.fileids():
        print(file)

def practice0():
    print("----------------------------")
    print("Practice 0")
    #datafile_path = '../data/ru_ar_cut.txt'
    #ng = NGCalculator(datafile_path)
    #print(ng.top30_mi())
    demo0()


def practice1():
    print("----------------------------")
    print("Practice 1")
    demo1()


def practice2():
    print("----------------------------")
    print("Practice 2")
    demo2()


def practice4():
    print("----------------------------")
    print("Practice 4")
    demo4()


if __name__ == '__main__':
    #practice0()
    #practice1()
    #practice2()
    practice4()

