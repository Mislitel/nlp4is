#!/usr/bin/env python 


DATASETS_DIR       = "../common_data"
FASTTEXT           = "../bin/fasttext"
TOPIC_RESULT_FILE  = "../topic_result.txt"
GENERAL_TOPIC_RESULT_FILE  = "../general_topic_result.txt"


from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords


def preprocessing_text(text):
    words = [word.lower() for word in word_tokenize(text) if word.isalpha()]
    return " ".join(words)


if __name__ == "__main__":

    import subprocess
    import json

    from os import listdir

    with open(TOPIC_RESULT_FILE, "w+") as file_topic, open(GENERAL_TOPIC_RESULT_FILE, "w+") as file_general_topic:
        for file in listdir(DATASETS_DIR):
            if file.endswith(".txt"):
                text_file = DATASETS_DIR + "/" + file
                json_file = DATASETS_DIR + "/" + file[:-4] + "_metadata.json"
                print(json_file)
                with open(json_file) as f:
                    j = json.load(f)
                    topic = j["topic"]
                    general_topic = j["general_topic"]
                print(text_file)
                with open(text_file) as f:
                    text = f.read()
                preprocessed_text = preprocessing_text(text) + '\n'
                file_general_topic.write("__label__" + general_topic + " __label__" + topic + " " + preprocessed_text)
                file_topic.write(" __label__" + topic + " " + preprocessed_text)

