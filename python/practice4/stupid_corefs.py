#!/usr/bin/env python
import spacy
import ast


def standard_associated(domain, standard):
    return bool(domain[0] == standard[0] and (set(domain[1].split()) & set(standard[1].split())))


def score(domains, standards):
    for item in domains:
        print(item)
    print()
    correct = 0
    incorrect = 0
    total = 0
    while domains:
        is_correct = False
        for i in range(len(standards)):
            if standard_associated(domains[0], standards[i]):
                standards = standards[i+1:]
                is_correct = True
                break
        if is_correct:
            correct += 1
        else:
            incorrect += 1
        total += 1
        domains = domains[1:]

    print("Correct: " + str(correct))
    print("Incorrect: " + str(incorrect))
    print("Total: " + str(total))
    score = 0 if total == 0 else correct / total
    print()
    print("Score: " + str(score))


def demo():
    # путь зависит от того, где находится вызываемый скрипт
    if __name__ == "__main__":
        data_prefix = '../../data/practice4'
    else:
        data_prefix = '../data/practice4'

    with open(data_prefix + '/big-ben.txt') as file:
        text = file.read()

    print("Original text:")
    print(text)
    print('\n')

    # файл с экспертным мнением
    with open(data_prefix + '/coref.expert') as file:
        standard_corefs = ast.literal_eval(file.read())

    # Убираем символы переноса строки, которые смущают spacy.
    # Чтобы проверить работоспособность оценки, можно закомментировать
    # и посмотреть, как будет вести себя spacy
    text = text.replace("\n", " ")
    nlp = spacy.load('en_core_web_sm')
    doc = nlp(text)

    entities = [entity.text for entity in doc.ents]

    result = []
    corefs = []
    last_entity = ""
    for token in doc:
        if token.pos_ == "PRON":
            result.append(last_entity)
            corefs.append([token.text, last_entity])
            continue
        if token.text in entities:
            last_entity = token.text
        result.append(token.text)

    coref = spacy.load('en_coref_sm')
    doc = coref(text)

    spacy_corefs = []
    for i in range(len(doc)):
        span = doc[i:i+1]
        if span._.is_coref:
            spacy_corefs.append([span.text, str(span._.coref_cluster.main)])

    print("Custom corefs:")
    score(corefs, standard_corefs)

    print('\n')
    print("Spacy corefs:")
    score(spacy_corefs, standard_corefs)


if __name__ == "__main__":
    demo()

