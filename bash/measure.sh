#!/usr/bin/env bash

TOPIC_RESULT_FILE="../../topic_result.txt"
GENERAL_TOPIC_RESULT_FILE="../../general_topic_result.txt"
WORKDIR_TOPIC="topic_result.txt"
WORKDIR_GENERAL_TOPIC="general_topic_result.txt"

function processing {
    echo $2
    cp $1 .
    count=$(wc $2 | awk '{print $1}')
    train=$(expr $count / 10 \* 7)
    valid=$(expr $count - $train)
    train_file="$2.train"
    valid_file="$2.valid"
    head -n $train $2 > $train_file
    tail -n $valid $2 > $valid_file
    ./fasttext supervised -input $train_file -output $2
    ./fasttext test $2.bin $valid_file
}

processing $TOPIC_RESULT_FILE $WORKDIR_TOPIC
processing $GENERAL_TOPIC_RESULT_FILE $WORKDIR_GENERAL_TOPIC

