#!/usr/bin/env bash

PRACTICE3_PATH=../data/practice3
WAR_AND_PEACE_FILENAME=war-and-peace-ru.txt
WAR_AND_PEACE_PATH=$PRACTICE3_PATH/$WAR_AND_PEACE_FILENAME
WAR_AND_PEACE_URI=http://files.libedu.ru/g5qkbyduggcf9zzimt559q1no5uig8rj/tolstoi_l_n__voina_i_mir.txt
TEMP_FILE=$PRACTICE3_PATH/waprutemp.txt

echo "Checking directory..."
if [[ ! -d $PRACTICE3_PATH ]]; then
    mkdir $PRACTICE3_PATH || exit
    echo "Directory created!"
fi

echo "Checking data file..."
if [[ ! -f $WAR_AND_PEACE_PATH ]]; then
    wget -O $TEMP_FILE $WAR_AND_PEACE_URI || exit
    iconv -f WINDOWS-1251 -t utf-8 -o $WAR_AND_PEACE_PATH $TEMP_FILE || exit
    rm $TEMP_FILE
    echo "File downloaded!"
fi

PATH_DAE=../../digitalhumanities_dataset_and_eval
PATH_NWE=../../nlp4is_word_embeddings

if [[ ! -d $PATH_NWE ]]; then
    git clone https://github.com/gwohlgen/nlp4is_word_embeddings $PATH_NWE
fi

if [[ ! -d $PATH_DAE ]]; then
    git clone https://github.com/gwohlgen/digitalhumanities_dataset_and_eval $PATH_DAE
fi
