#!/usr/bin/env bash

ARCHIVE_TITLE=mystem-3.1-linux-64bit.tar.gz
BIN_PATH=../bin
MYSTEM_PATH=$BIN_PATH/mystem
DATA_PATH=../data/ru_ar_cut_preproceed.txt
RESULT_PATH=../data/ru_ar_cut.json

if [[ ! -d $BIN_PATH ]]; then
    mkdir $BIN_PATH || exit
fi

if [[ ! -f $MYSTEM_PATH ]]; then
    wget http://download.cdn.yandex.net/mystem/$ARCHIVE_TITLE
    tar -xvzf $ARCHIVE_TITLE -C $BIN_PATH
    rm $ARCHIVE_TITLE
fi

echo "mystem in processing..."
$MYSTEM_PATH --format=json -gin $DATA_PATH $RESULT_PATH
echo "mystem has completed!"
