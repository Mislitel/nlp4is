#!/usr/bin/env bash

PRACTICE2_PATH=../data/practice2
THOTB_FILENAME=the_hound_of_the_baskervilles.txt
THOTB_PATH=$PRACTICE2_PATH/$THOTB_FILENAME
THOTB_URI=https://www.gutenberg.org/files/2852/2852-0.txt

echo "Checking directory..."
if [[ ! -d $PRACTICE2_PATH ]]; then
    mkdir $PRACTICE2_PATH || exit
    echo "Directory created!"
fi

echo "Checking data file..."
if [[ ! -f $THOTB_PATH ]]; then
    wget -O $THOTB_PATH $THOTB_URI || exit
    echo "File downloaded!"
fi
