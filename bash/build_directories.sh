#!/usr/bin/env bash

PREFIX_DATA=../data/practice0
PREFIX_PYTHON=../python/practice0
CONDA_ENV_TITLE=NLP4IS
SOURCES_FILE=sources
CONDA_PATH=/opt/miniconda3

if [[ ! -d $PREFIX_DATA ]]; then
    mkdir $PREFIX_DATA || exit
fi

# проверка окружения conda
if [[ "$CONDA_DEFAULT_ENV" != "$CONDA_ENV_TITLE" ]]; then
    echo "Conda Environment '"$CONDA_ENV_TITLE"' not found. Please, activate it using 'conda activate "$CONDA_ENV_TITLE"'"
    exit
fi
echo "$CONDA_DEFAULT_ENV == $CONDA_ENV_TITLE"
while read LINE; do
    # строка - комментарий, ничего не делаем
    if [[ $LINE =~ \#.* ]]; then
        continue
    fi
    # строка - информация об источнике подсобытия
    # формат:
    # subevent_id subevent_uri <topic> <pub_date> <event_date>
    reg_temp=\(.+\)\ \(http.*\)\ \<\(.*\)\>\ \<\(.*\)\>\ \<\(.*\)\>
    if [[ $LINE =~ $reg_temp ]]; then
        if [[ -z $event ]]; then
            echo "File '"$SOURCES_FILE"' is incorrect: no event title"
            break
        fi
        subevent=${BASH_REMATCH[1]}
        subevent_uri=${BASH_REMATCH[2]}
        subevent_topic=${BASH_REMATCH[3]}
        subevent_publishing_date=${BASH_REMATCH[4]}
        subevent_event_date=${BASH_REMATCH[5]}

        subevent_path=$event_path/$subevent
        if [[ ! -d $subevent_path ]]; then
            mkdir $subevent_path || continue
        fi

        subevent_html=$subevent_path/$subevent.html
        if [[ ! -f $subevent_html ]]; then
            echo $subevent" is downloading from: "$subevent_uri
            wget -O $subevent_html $subevent_uri || continue
        fi

        subevent_text=$subevent_path/$subevent.txt
        if [[ ! -f $subevent_text ]]; then
            echo "Creatind file for text (manual filling require): "$subevent_text
            touch $subevent_text
        fi

        subevent_metadata=$subevent_path/$(echo $subevent)_metadata.json
        if [[ -f $subevent_metadata ]]; then
            echo "Creating metadata file: "$subevent_metadata
        else
            echo "Rewriting metadata file: "$subevent_metadata
        fi
        echo '{
            "URI": "'$subevent_uri'",
            "title": "'$(python $PREFIX_PYTHON/find_title.py $subevent_path/$subevent.html)'",
            "publishing_date": "'$subevent_publishing_date'",
            "event_date": "'$subevent_event_date'",
            "previous_subevent_uri": '$previous_subevent_uri',
            "topic": "'$subevent_topic'",' > $subevent_metadata
        if [[ -n $previous_subevent_metadata ]]; then
            echo "Updating previous metadata file: "$previous_subevent_metadata
            echo '"next_subevent_uri": "'$subevent_uri'" }' >> $previous_subevent_metadata
        fi
        previous_subevent_uri='"'$subevent_uri'"'
        previous_subevent_metadata=$subevent_metadata
        continue
    fi

    # строка - идентификатор события
    # берём первое слово в строке в качестве идентификатора
    event=$(echo $LINE | awk '{ print $1 }')
    if [[ -n $event ]]; then
        echo "Event "$event
        previous_subevent_uri=null
        if [[ -n $previous_subevent_metadata ]]; then
            echo '"next_subevent_uri": null }' >> $previous_subevent_metadata
            previous_subevent_metadata=
        fi
        event_path=$PREFIX_DATA/$event
        if [[ -d "$event_path" ]]; then
            echo "Directory '"$event_path"' exists"
        else
            echo "Creating directory '"$event_path"'"
            mkdir $event_path || break
        fi
    fi
done < $SOURCES_FILE

if [[ -n $subevent_metadata ]]; then
    echo '"next_subevent_uri": null }' >> $subevent_metadata
fi

